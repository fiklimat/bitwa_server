﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class OrderService : IOrderService
    {
        public unprocessed_order createOrder(unprocessed_order order)
        {
            using (var db = new AppDbContext())
            {
                foreach (var item in order.unprocessed_order_item)
                {
                    var temp = db.item.Find(item.item_id);
                    temp.stock = db.stock.Find(item.item_id);
                    if (temp.stock.quantity >= item.quantity)
                    {
                        db.stock.Attach(temp.stock);
                        temp.stock.quantity -= item.quantity;
                    }
                    else
                        return null;

                }
                db.unprocessed_order.Add(order);
                db.SaveChanges();
                return order;
            }
        }

        public processed_order CreateProcessedOrder(unprocessed_order order)
        {
            using (var db = new AppDbContext())
            {
                processed_order processedOrder = new processed_order();
                processedOrder.name = order.name;
                processedOrder.surname = order.surname;
                processedOrder.street = order.street;
                processedOrder.number = order.number;
                processedOrder.town = order.town;
                processedOrder.post_code = order.post_code;
                processedOrder.email = order.email;
                processedOrder.phone_number = order.phone_number;
                processedOrder.total_price = order.total_price;
                processedOrder.processed = DateTime.Now;
                db.processed_order.Add(processedOrder);
                db.SaveChanges();
                foreach (var item in order.unprocessed_order_item)
                {
                    processed_order_item processedItem = new processed_order_item();
                    processedItem.processed_order_id = processedOrder.id;
                    processedItem.item_id = item.item_id;
                    processedItem.final_price_per_item = item.final_price_per_item;
                    processedItem.quantity = item.quantity;
                    processedOrder.processed_order_item.Add(processedItem);
                    db.processed_order_item.Add(processedItem);
                }
                db.SaveChanges();
                return processedOrder;
            }
        }
        public void DeleteUnprocessedOrder(unprocessed_order order)
        {
            using (var db = new AppDbContext())
            {

                for (int i = 0; i < order.unprocessed_order_item.Count; i++)
                {
                    db.unprocessed_order_item.Attach(order.unprocessed_order_item.ElementAt(i));
                    db.unprocessed_order_item.Remove(order.unprocessed_order_item.ElementAt(i));
                }
                db.unprocessed_order.Attach(order);
                db.unprocessed_order.Remove(order);
                db.SaveChanges();
            }
        }

        public unprocessed_order FindUnprocessedOrder(int id)
        {
            using (var db = new AppDbContext())
            {
                var order = db.unprocessed_order.Find(id);
                var items = db.unprocessed_order_item.Where(i => (i.unprocessed_order_id == id)).ToList();
                order.unprocessed_order_item = items;
                return order;
            }
        }

        public List<processed_order> GetProecessedOrders()
        {
            using (var db = new AppDbContext())
            {
                var orders = db.processed_order.ToList();
                var items = db.processed_order_item.ToList();
                foreach (var order in orders)
                {
                    order.processed_order_item = items.Where(i => (i.processed_order_id == order.id)).ToList();
                }
                return orders;
            }
        }

        public List<unprocessed_order> GetUnproecessedOrders()
        {
            using (var db = new AppDbContext())
            {
                var orders = db.unprocessed_order.ToList();
                var items = db.unprocessed_order_item.ToList();
                foreach (var order in orders)
                {
                    order.unprocessed_order_item = items.Where(i => (i.unprocessed_order_id == order.id)).ToList();
                }
                return orders;
            }
        }
    }
}