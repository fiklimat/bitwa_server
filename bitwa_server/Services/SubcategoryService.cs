﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class SubcategoryService : ISubcategoryService
    {
        public List<subcategory> GetSubcategoriesForCategory(string category)
        {
            using (var db = new AppDbContext())
            {
                var subcategories = db.subcategory.Where(sct => sct.category.name == category).ToList();
                foreach (var subcategory in subcategories)
                    subcategory.category = db.category.Where(ct => ct.id == subcategory.category_id).FirstOrDefault();
                return subcategories;
            }
        }
    }
}