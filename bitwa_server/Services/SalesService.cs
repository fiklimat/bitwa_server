﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class SalesService : ISalesService
    {
        public void AddSale(DateTime date, int number)
        {
            using (var db = new AppDbContext())
            {
                sales sale = db.sales.Find(date.Date);
                if (sale != null)
                {
                    db.sales.Attach(sale);
                    sale.number += number;
                    db.SaveChanges();
                }
                else
                {
                    sale = new sales();
                    sale.date = DateTime.Now.Date;
                    sale.number = number;
                    db.sales.Add(sale);
                    db.SaveChanges();

                }
            }
        }

        public List<string> GetDates()
        {
            using (var db = new AppDbContext()) {
                List<sales> sales = db.sales.ToList();
                List<string> dates = new List<string>();
                foreach (sales sale in sales) {
                    dates.Add(sale.date.ToString());
                }
                return dates;
            }
        }

        public List<sales> GetSales(List<DateTime> requestedDates)
        {
            using (var db = new AppDbContext())
            {
                List<sales> sales = new List<sales>();
                foreach (DateTime date in requestedDates)
                {
                    sales sale = db.sales.Find(date);
                    sales.Add(sale);
                }
                return sales;
            }
        }
    }
}