﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace bivaa_server.Services
{
    public class CommonService : ICommonService
    {
        public HttpResponseMessage GetResponse(int statusCode)
        {
            var result = new HttpResponseMessage();
            result.ApplyStatusCode(statusCode);

            return result;
        }

        public HttpResponseMessage GetResponse(string content)
        {
            var result = GetBaseResponse(content);
            result.ApplyJsonContentType();
            return result;
        }

        public HttpResponseMessage GetResponse(string content, int statusCode)
        {
            var result = GetBaseResponse(content);
            result.ApplyJsonContentType();
            result.ApplyStatusCode(statusCode);
            return result;
        }
        private HttpResponseMessage GetBaseResponse(string content)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(content)
            };
        }
    }
}