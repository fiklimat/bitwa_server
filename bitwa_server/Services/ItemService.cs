﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class ItemService : IItemService
    {
        public item GetItemDetail(int id)
        {
            using (var db = new AppDbContext())
            {
                var item = db.item.Find(id);
                var stock = db.stock.Find(item.id);
                item.stock = stock;
                var item_subcategories = db.item_subcategory.Where(isub => isub.item_id == item.id).ToList();
                foreach (var subcategory in item_subcategories)
                {
                    subcategory.subcategory = db.subcategory.Find(subcategory.subcategory_id);
                    item.item_subcategory.Add(subcategory);
                }
                return item;
            }
        }

        public List<item> GetItems()
        {
            using (var db = new AppDbContext())
            {
                var items = db.item.ToList();
                var items_to_remove = new List<item>();
                foreach (var item in items)
                {
                    var stock = db.stock.Find(item.id);
                    item.stock = stock;
                    if (stock.quantity == 0)
                        items_to_remove.Add(item);
                    var item_subcategories = db.item_subcategory.Where(isub => isub.item_id == item.id).ToList();
                    foreach (var subcategory in item_subcategories)
                        item.item_subcategory.Add(subcategory);
                }
                foreach (var item in items_to_remove)
                    items.Remove(item);
                return items;
            }
        }

        public List<item> GetItems(List<unprocessed_order> unprocessedOrders)
        {
            using (var db = new AppDbContext())
            {
                List<item> items = new List<item>();
                foreach (var order in unprocessedOrders)
                {
                    foreach (var shortItem in order.unprocessed_order_item)
                    {
                        item item = new item();
                        item.id = shortItem.item_id;
                        item.name = db.item.Find(shortItem.item_id).name;
                        items.Add(item);
                    }
                }
                return items;
            }
        }

        public List<item> GetItems(List<processed_order> processedOrders)
        {
            using (var db = new AppDbContext())
            {
                List<item> items = new List<item>();
                foreach (var order in processedOrders)
                {
                    foreach (var shortItem in order.processed_order_item)
                    {
                        item item = new item();
                        item.id = shortItem.item_id;
                        item.name = db.item.Find(shortItem.item_id).name;
                        items.Add(item);
                    }
                }
                return items;
            }
        }

        public List<item> GetItemsByCategory(string category)
        {
            using (var db = new AppDbContext())
            {
                int category_id = db.category.Where(c => c.name == category).FirstOrDefault().id;
                var items = db.item.Where(i => i.category_id == category_id).ToList();
                var items_to_remove = new List<item>();
                foreach (var item in items) 
                {
                    var stock = db.stock.Find(item.id);
                    item.stock = stock;
                    if (stock.quantity == 0)
                        items_to_remove.Add(item);
                    var item_subcategories = db.item_subcategory.Where(isub => isub.item_id == item.id).ToList();
                    foreach (var subcategory in item_subcategories)
                        item.item_subcategory.Add(subcategory);
                }
                foreach (var item in items_to_remove)
                    items.Remove(item);
                return items;
            }
        }
    }
}