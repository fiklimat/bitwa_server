﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Services
{
    public class UserService : IUserService
    {
        public bool CheckUser(string username)
        {
            using (var db = new AppDbContext())
            {
                var user = db.user.Find(username);
                return user != null;
            }
        }
    }
}