﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IOrderService
    {
        unprocessed_order createOrder(unprocessed_order order);
        List<unprocessed_order> GetUnproecessedOrders();
        unprocessed_order FindUnprocessedOrder(int id);
        void DeleteUnprocessedOrder(unprocessed_order order);
        processed_order CreateProcessedOrder(unprocessed_order order);
        List<processed_order> GetProecessedOrders();
    }
}