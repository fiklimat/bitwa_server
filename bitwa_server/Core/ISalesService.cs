﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface ISalesService
    {
        List<string> GetDates();
        List<sales> GetSales(List<DateTime> requestedDates);
        void AddSale(DateTime date, int number);
    }
}