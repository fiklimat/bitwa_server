﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IUserService
    {
        bool CheckUser(string username);
    }
}