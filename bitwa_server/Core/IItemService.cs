﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bivaa_server.Core
{
    public interface IItemService
    {
        List<item> GetItemsByCategory(string category);
        List<item> GetItems();
        List<item> GetItems(List<unprocessed_order> unprocessedOrders);
        List<item> GetItems(List<processed_order> processedOrders);
        item GetItemDetail(int id);
    }
}