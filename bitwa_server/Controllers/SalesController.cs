﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class SalesController : BaseController
    {
        private ISalesService salesService;

        public SalesController(ISalesService salesService, ICommonService commonService) : base(commonService)
        {
            this.salesService = salesService;
        }

        [Authorize]
        [HttpGet]
        [Route("api/sales/dates/all")]
        public HttpResponseMessage GetDates()
        {
            try
            {
                List<string> dates = salesService.GetDates();
                if (dates.Count == 0)
                    return commonService.GetResponse(404);
                else
                    return commonService.GetResponse(dates.DatesToJson().ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }

        [Authorize]
        [HttpPost]
        [Route("api/sales/filtered/date")]
        public HttpResponseMessage GetSalesByDates(HttpRequestMessage req)
        {
            try
            {
                string requestBody = req.Content.ReadAsStringAsync().Result;
                List<DateTime> requestedDates = requestBody.DatesFromJson();
                List<sales> sales = salesService.GetSales(requestedDates);
                if (sales.Count == 0)
                    return commonService.GetResponse(404);
                else
                    return commonService.GetResponse(sales.SalesToJson().ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }
    }
}