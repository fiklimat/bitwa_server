﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class UserController : BaseController
    {
        private IUserService userService;

        public UserController(IUserService userService, ICommonService commonService) : base(commonService)
        {
            this.userService = userService;
        }

        [Authorize]
        [HttpGet]
        [Route("api/user/detail/username/{username}")]
        public HttpResponseMessage GetSubcategoriesForCategory(string username)
        {
            try
            {
                bool found = userService.CheckUser(username);
                if (!found)
                    return commonService.GetResponse(404);
                return commonService.GetResponse(username.ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }
    }
}