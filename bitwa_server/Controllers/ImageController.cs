﻿using bivaa_server.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class ImageController : BaseController
    {
        public ImageController(ICommonService commonService) : base(commonService)
        {
        }

        [HttpGet]
        public HttpResponseMessage GetImageById(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string filePath = "/images/";
                string fullPath = AppDomain.CurrentDomain.BaseDirectory + filePath + "/" + id + ".jpg";
                if (File.Exists(fullPath))
                {

                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    var fileStream = new FileStream(fullPath, FileMode.Open);
                    response.Content = new StreamContent(fileStream);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = id;
                    return response;
                }
            }

            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}