﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class SubcategoryController : BaseController
    {
        private ISubcategoryService subcategoryService;
        public SubcategoryController(ICommonService commonService, ISubcategoryService subcategoryService) : base(commonService)
        {
            this.subcategoryService = subcategoryService;
        }


        [HttpGet]
        [Route("api/subcategory/category/{category}")]
        public HttpResponseMessage GetSubcategoriesForCategory(string category)
        {
            try
            {
                var subcategories = subcategoryService.GetSubcategoriesForCategory(category);
                if (subcategories == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(subcategories.SubcategoriesToJson().ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }
    }
}