﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class OrderController : BaseController
    {
        private IOrderService orderService;
        private IItemService itemService;
        private ISalesService salesService;

        public OrderController(IOrderService orderService, IItemService itemService, ISalesService salesService, ICommonService commonService) : base(commonService)
        {
            this.orderService = orderService;
            this.itemService = itemService;
            this.salesService = salesService;
        }

        [HttpPost]
        [ActionName("create")]
        public HttpResponseMessage Create(HttpRequestMessage req)
        {
            try
            {
                string requestBody = req.Content.ReadAsStringAsync().Result;
                unprocessed_order requestOrder = requestBody.OrderFromJson();
                requestOrder.created = DateTime.Now;
                var createdOrder = orderService.createOrder(requestOrder);
                if (createdOrder == null)
                    return commonService.GetResponse(409);
                else
                    return commonService.GetResponse(createdOrder.id.ToString());
            }
            catch { return commonService.GetResponse(500); }
        }
        
        [Authorize]
        [HttpGet]
        [Route("api/orders/unprocessed/all")]
        public HttpResponseMessage GetUnprocessedOrders()
        {
            try
            {
                List<unprocessed_order> unprocessedOrders = orderService.GetUnproecessedOrders();
                List<item> items = itemService.GetItems(unprocessedOrders);
                if (unprocessedOrders == null)
                    return commonService.GetResponse(204);
                else
                    return commonService.GetResponse(unprocessedOrders.UnprocessedOrdersToJson(items).ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }

        [Authorize]
        [HttpGet]
        [Route("api/orders/process/{id}")]
        public HttpResponseMessage GetUnprocessedOrders(int id)
        {
            try
            {
                unprocessed_order unprocessedOrder = orderService.FindUnprocessedOrder(id);
                if (unprocessedOrder == null)
                    return commonService.GetResponse(409);
                processed_order processedOrder = orderService.CreateProcessedOrder(unprocessedOrder);
                salesService.AddSale(processedOrder.processed, processedOrder.total_price);
                orderService.DeleteUnprocessedOrder(unprocessedOrder);
                return commonService.GetResponse(200);
            }
            catch { return commonService.GetResponse(500); }
        }

        [Authorize]
        [HttpGet]
        [Route("api/orders/processed/all")]
        public HttpResponseMessage GetProcessedOrders()
        {
            try
            {
                List<processed_order> processedOrders = orderService.GetProecessedOrders();
                List<item> items = itemService.GetItems(processedOrders);
                if (processedOrders == null)
                    return commonService.GetResponse(204);
                else
                    return commonService.GetResponse(processedOrders.ProcessedOrdersToJson(items).ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }
    }
}