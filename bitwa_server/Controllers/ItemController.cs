﻿using bivaa_server.Core;
using bivaa_server.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace bivaa_server.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    public class ItemController : BaseController
    {
        private IItemService itemService;

        public ItemController(IItemService itemService, ICommonService commonService) : base(commonService)
        {
            this.itemService = itemService;
        }

        [HttpGet]
        [ActionName("all")]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var items = itemService.GetItems();
                if (items == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(items.ItemsToJson().ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }

        [HttpGet]
        [Route("api/item/category/{category}")]
        public HttpResponseMessage GetItemsByCategory(string category)
        {
            try
            {
                var items = itemService.GetItemsByCategory(category);
                if (items == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(items.ItemsToJson().ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }

        [HttpGet]
        [Route("api/item/detail/{id}")]
        public HttpResponseMessage GetItemDetail(int id)
        {
            try
            {
                var item = itemService.GetItemDetail(id);
                if (item == null)
                    return commonService.GetResponse(204);
                return commonService.GetResponse(item.ItemToJson().ConvertToString());
            }
            catch { return commonService.GetResponse(500); }
        }
    }
}