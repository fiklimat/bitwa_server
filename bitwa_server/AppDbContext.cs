using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace bivaa_server
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("name=AppDbContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<category> category { get; set; }
        public virtual DbSet<item> item { get; set; }
        public virtual DbSet<processed_order> processed_order { get; set; }
        public virtual DbSet<processed_order_item> processed_order_item { get; set; }
        public virtual DbSet<sales> sales { get; set; }
        public virtual DbSet<stock> stock { get; set; }
        public virtual DbSet<subcategory> subcategory { get; set; }
        public virtual DbSet<unprocessed_order> unprocessed_order { get; set; }
        public virtual DbSet<unprocessed_order_item> unprocessed_order_item { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<item_subcategory> item_subcategory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<category>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<category>()
                .HasMany(e => e.item)
                .WithRequired(e => e.category)
                .HasForeignKey(e => e.category_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<category>()
                .HasMany(e => e.subcategory)
                .WithRequired(e => e.category)
                .HasForeignKey(e => e.category_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<item>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<item>()
                .Property(e => e.short_description)
                .IsUnicode(false);

            modelBuilder.Entity<item>()
                .Property(e => e.long_description)
                .IsUnicode(false);

            modelBuilder.Entity<item>()
                .HasMany(e => e.item_subcategory)
                .WithRequired(e => e.item)
                .HasForeignKey(e => e.item_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<item>()
                .HasMany(e => e.processed_order_item)
                .WithRequired(e => e.item)
                .HasForeignKey(e => e.item_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<item>()
                .HasOptional(e => e.stock)
                .WithRequired(e => e.item);

            modelBuilder.Entity<item>()
                .HasMany(e => e.unprocessed_order_item)
                .WithRequired(e => e.item)
                .HasForeignKey(e => e.item_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<processed_order>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<processed_order>()
                .Property(e => e.surname)
                .IsUnicode(false);

            modelBuilder.Entity<processed_order>()
                .Property(e => e.street)
                .IsUnicode(false);

            modelBuilder.Entity<processed_order>()
                .Property(e => e.town)
                .IsUnicode(false);

            modelBuilder.Entity<processed_order>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<processed_order>()
                .Property(e => e.phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<processed_order>()
                .HasMany(e => e.processed_order_item)
                .WithRequired(e => e.processed_order)
                .HasForeignKey(e => e.processed_order_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<subcategory>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<subcategory>()
                .HasMany(e => e.item_subcategory)
                .WithRequired(e => e.subcategory)
                .HasForeignKey(e => e.subcategory_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<unprocessed_order>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<unprocessed_order>()
                .Property(e => e.surname)
                .IsUnicode(false);

            modelBuilder.Entity<unprocessed_order>()
                .Property(e => e.street)
                .IsUnicode(false);

            modelBuilder.Entity<unprocessed_order>()
                .Property(e => e.town)
                .IsUnicode(false);

            modelBuilder.Entity<unprocessed_order>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<unprocessed_order>()
                .Property(e => e.phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<unprocessed_order>()
                .HasMany(e => e.unprocessed_order_item)
                .WithRequired(e => e.unprocessed_order)
                .HasForeignKey(e => e.unprocessed_order_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<user>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<item_subcategory>()
                .Property(e => e.value)
                .IsUnicode(false);
        }
    }
}
