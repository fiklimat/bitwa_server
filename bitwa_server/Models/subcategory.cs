namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("subcategory")]
    public partial class subcategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public subcategory()
        {
            item_subcategory = new HashSet<item_subcategory>();
        }

        public int id { get; set; }

        public int category_id { get; set; }

        [Required]
        [StringLength(255)]
        public string name { get; set; }

        public virtual category category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<item_subcategory> item_subcategory { get; set; }
    }
}
