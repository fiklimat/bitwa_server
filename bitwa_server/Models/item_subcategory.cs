namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("item_subcategory")]
    public partial class item_subcategory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int item_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int subcategory_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(255)]
        public string value { get; set; }

        public virtual item item { get; set; }

        public virtual subcategory subcategory { get; set; }
    }
}
