namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processed_order_item")]
    public partial class processed_order_item
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int processed_order_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int item_id { get; set; }

        public int final_price_per_item { get; set; }

        public int quantity { get; set; }

        public virtual item item { get; set; }

        public virtual processed_order processed_order { get; set; }
    }
}
