namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("unprocessed_order")]
    public partial class unprocessed_order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public unprocessed_order()
        {
            unprocessed_order_item = new HashSet<unprocessed_order_item>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string name { get; set; }

        [Required]
        [StringLength(255)]
        public string surname { get; set; }

        [Required]
        [StringLength(255)]
        public string street { get; set; }

        public int number { get; set; }

        [Required]
        [StringLength(255)]
        public string town { get; set; }

        public int post_code { get; set; }

        [Required]
        [StringLength(255)]
        public string email { get; set; }

        [Required]
        [StringLength(255)]
        public string phone_number { get; set; }

        public int total_price { get; set; }

        public DateTime created { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<unprocessed_order_item> unprocessed_order_item { get; set; }
    }
}
