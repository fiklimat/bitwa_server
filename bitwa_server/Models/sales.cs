namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("sales")]
    public partial class sales
    {
        [Key]
        [Column(TypeName = "date")]
        public DateTime date { get; set; }

        public int? number { get; set; }
    }
}
