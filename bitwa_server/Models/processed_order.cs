namespace bivaa_server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("processed_order")]
    public partial class processed_order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public processed_order()
        {
            processed_order_item = new HashSet<processed_order_item>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string name { get; set; }

        [Required]
        [StringLength(255)]
        public string surname { get; set; }

        [Required]
        [StringLength(255)]
        public string street { get; set; }

        public int number { get; set; }

        [Required]
        [StringLength(255)]
        public string town { get; set; }

        public int post_code { get; set; }

        [Required]
        [StringLength(255)]
        public string email { get; set; }

        [Required]
        [StringLength(255)]
        public string phone_number { get; set; }

        public int total_price { get; set; }

        public DateTime processed { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<processed_order_item> processed_order_item { get; set; }
    }
}
