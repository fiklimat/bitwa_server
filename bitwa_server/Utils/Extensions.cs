﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace bivaa_server.Utils
{
    public static class Extensions
    {
        public static JObject ItemToJson(this item item)
        {
            dynamic result = new JObject();
            result.id = item.id;
            result.name = item.name;
            result.short_description = item.short_description;
            result.long_description = item.long_description;
            result.price = item.price;
            result.quantity = item.stock.quantity;
            result.subcategories = new JArray();
            foreach (var subcategory in item.item_subcategory)
            {
                dynamic entry = new JObject();
                entry.subcategory_id = subcategory.subcategory_id;
                entry.subcategory_name = subcategory.subcategory.name;
                entry.value = subcategory.value;
                result.subcategories.Add(entry);
            }
            return result;
        }
        public static JArray ItemsToJson(this List<item> items)
        {
            dynamic result = new JArray();
            foreach (var item in items)
            {
                dynamic jObject = new JObject();
                jObject.id = item.id;
                jObject.name = item.name;
                jObject.price = item.price;
                jObject.quantity = item.stock.quantity;
                jObject.subcategories = new JArray();
                foreach (var subcategory in item.item_subcategory)
                {
                    dynamic entry = new JObject();
                    entry.subcategory_id = subcategory.subcategory_id;
                    entry.value = subcategory.value;
                    jObject.subcategories.Add(entry);
                }
                result.Add(jObject);
            }
            return result;
        }
        public static JArray SubcategoriesToJson(this List<subcategory> subcategories)
        {
            dynamic result = new JArray();
            foreach (var subcategory in subcategories)
            {
                dynamic jObject = new JObject();
                jObject.id = subcategory.id;
                jObject.category_id = subcategory.category_id;
                jObject.category_name = subcategory.category.name;
                jObject.name = subcategory.name;
                result.Add(jObject);
            }
            return result;
        }
        public static unprocessed_order OrderFromJson(this string request)
        {
            JObject obj = JObject.Parse(request);
            string jsonUnprocessedOrder = obj["billing_information"].ToString();
            string jsonItems = obj["items"].ToString();
            unprocessed_order unprocessed_order = JsonConvert.DeserializeObject<unprocessed_order>(jsonUnprocessedOrder);
            List<unprocessed_order_item> unprocessed_order_items = JsonConvert.DeserializeObject<List<unprocessed_order_item>>(jsonItems);
            unprocessed_order.unprocessed_order_item = unprocessed_order_items;
            return unprocessed_order;
        }
        public static JArray DatesToJson(this List<string> dates)
        {
            dynamic result = new JArray();
            foreach (var date in dates)
            {
                dynamic jObject = new JObject();
                jObject.date = date;
                result.Add(jObject);
            }
            return result;
        }
        public static List<DateTime> DatesFromJson(this string request)
        {
            JArray arr = JArray.Parse(request);
            List<DateTime> dates = new List<DateTime>();
            foreach (string obj in arr) {
                dates.Add(DateTime.Parse(obj));
            }
            return dates;
        }
        public static JArray SalesToJson(this List<sales> sales)
        {
            dynamic result = new JArray();
            foreach (sales sale in sales)
            {
                dynamic obj = new JObject();
                obj.date = sale.date.ToString("dd.MM.yyyy");
                obj.number = sale.number;
                result.Add(obj);
            }
            return result;
        }
        public static JObject ProcessedOrderToJson(this processed_order order, List<item> items)
        {
            dynamic result = new JObject();
            result.id = order.id;
            result.name = order.name;
            result.surname = order.surname;
            result.street = order.street;
            result.number = order.number;
            result.town = order.town;
            result.post_code = order.post_code;
            result.email = order.email;
            result.phone_number = order.phone_number;
            result.total_price = order.total_price;
            result.processed = order.processed.ToString("dd.MM.yyyy");
            result.processed_order_item = new JArray();
            foreach (var processed_order_item in order.processed_order_item)
            {
                dynamic item = new JObject();
                item.processed_order_id = processed_order_item.processed_order_id;
                item.item_id = processed_order_item.item_id;
                item.name = items.Find(i => (i.id == processed_order_item.item_id)).name;
                item.final_price_per_item = processed_order_item.final_price_per_item;
                item.quantity = processed_order_item.quantity;
                result.processed_order_item.Add(item);
            }
            return result;
        }
        public static JArray ProcessedOrdersToJson(this List<processed_order> orders, List<item> items)
        {
            dynamic result = new JArray();
            foreach (processed_order order in orders)
            {
                dynamic obj = order.ProcessedOrderToJson(items);
                result.Add(obj);
            }
            return result;
        }
        public static JObject UnprocessedOrderToJson(this unprocessed_order order, List<item> items)
        {
            dynamic result = new JObject();
            result.id = order.id;
            result.name = order.name;
            result.surname = order.surname;
            result.street = order.street;
            result.number = order.number;
            result.town = order.town;
            result.post_code = order.post_code;
            result.email = order.email;
            result.phone_number = order.phone_number;
            result.total_price = order.total_price;
            result.created = order.created.ToString("dd.MM.yyyy");
            result.unprocessed_order_item = new JArray();
            foreach (var unprocessed_order_item in order.unprocessed_order_item)
            {
                dynamic item = new JObject();
                item.unprocessed_order_id = unprocessed_order_item.unprocessed_order_id;
                item.item_id = unprocessed_order_item.item_id;
                item.name = items.Find(i => (i.id == unprocessed_order_item.item_id)).name;
                item.final_price_per_item = unprocessed_order_item.final_price_per_item;
                item.quantity = unprocessed_order_item.quantity;
                result.unprocessed_order_item.Add(item);
            }
            return result;
        }
        public static JArray UnprocessedOrdersToJson(this List<unprocessed_order> orders, List<item> items)
        {
            dynamic result = new JArray();
            foreach (unprocessed_order order in orders)
            {
                dynamic obj = order.UnprocessedOrderToJson(items);
                result.Add(obj);
            }
            return result;
        }
        public static string GetHash(this string input)
        {
            using (var sha = new System.Security.Cryptography.SHA256Managed())
            {
                byte[] textData = System.Text.Encoding.UTF8.GetBytes(input);
                byte[] hash = sha.ComputeHash(textData);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }
      
        public static string ConvertToString(this Object obj)
        {
            return Convert.ToString(obj);
        }

        public static void ApplyJsonContentType(this HttpResponseMessage msg, string mediaType = "application/json")
        {
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(mediaType);
        }
        public static void ApplyStatusCode(this HttpResponseMessage msg, int statusCode)
        {
            msg.StatusCode = (System.Net.HttpStatusCode)statusCode;
        }
    }
}